// -- ****************************************************************/
// -- ****************************************************************/
// --                                                 ,,             */
// --          `7MMM.     ,MMF'         `7MMF'        db             */
// --            MMMb    dPMM             MM                         */
// --            M YM   ,M MM   .gP"Ya    MM        `7MM             */
// --            M  Mb  M' MM  ,M'   Yb   MM          MM             */
// --            M  YM.P'  MM  8M""""""   MM      ,   MM             */
// --            M  `YM'   MM  YM.    ,   MM     ,M   MM             */
// --          .JML. `'  .JMML. `Mbmmd' .JMMmmmmMMM .JMML.           */
// -- ****************************************************************/
// -- ****************************************************************/
/* -- ****************************************************************/
/* --                    procesadorTexto.go                          */
/* --                                                                */
/* --  Descripcion: Definicion de estructura del ejercicio           */
/* --                                                                */
/* --   @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/* --                                                                */
/* --  © 2022 - Mercado Libre - Desafio Tecnico SOLID                */
/* -- ****************************************************************/

package procesadorTexto

func (p *ProcesadorTexto) Nueva(palabra string) {
	p.Texto = append(p.Texto, palabra)
}

func (p *ProcesadorTexto) ConcatTexto() string {
	var s string
	for _, cad := range p.Texto {
		if s == "" {
			s = cad
		} else {
			s = s + " " + cad
		}
	}
	return s
}

func (p *ProcesadorTexto) IsCorrecto(a []string) bool {
	var t string
	for _, w := range a {
		if t == "" {
			t = w
		} else {
			t = t + " " + w
		}
	}
	if t != p.ConcatTexto() {
		return false
	}
	return true
}
