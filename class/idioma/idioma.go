// -- ****************************************************************/
// -- ****************************************************************/
// --                                                 ,,             */
// --          `7MMM.     ,MMF'         `7MMF'        db             */
// --            MMMb    dPMM             MM                         */
// --            M YM   ,M MM   .gP"Ya    MM        `7MM             */
// --            M  Mb  M' MM  ,M'   Yb   MM          MM             */
// --            M  YM.P'  MM  8M""""""   MM      ,   MM             */
// --            M  `YM'   MM  YM.    ,   MM     ,M   MM             */
// --          .JML. `'  .JMML. `Mbmmd' .JMMmmmmMMM .JMML.           */
// -- ****************************************************************/
// -- ****************************************************************/
/* -- ****************************************************************/
/* --                          idioma.go                             */
/* --                                                                */
/* --  Descripcion: Definicion de palabras por idioma.               */
/* --                                                                */
/* --   @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/* --                                                                */
/* --  © 2022 - Mercado Libre - Desafio Tecnico SOLID                */
/* -- ****************************************************************/

package idioma

type Diccionario interface {
	GenerateDiccionario()
}

type IdiomaES struct {
	Language []string
}

type IdiomaEN struct {
	Language []string
}

type IdiomaPO struct {
	Language []string
}

func (i *IdiomaES) GenerateDiccionario() {
	var idioma []string
	idioma = append(idioma, "Tengo")
	idioma = append(idioma, "hambre")
	i.Language = idioma
}

func (i *IdiomaEN) GenerateDiccionario() {
	var idioma []string
	idioma = append(idioma, "I")
	idioma = append(idioma, "am")
	idioma = append(idioma, "angry")
	i.Language = idioma
}

func (i *IdiomaPO) GenerateDiccionario() {
	var idioma []string
	idioma = append(idioma, "Tenho")
	idioma = append(idioma, "fome")
	i.Language = idioma
}
