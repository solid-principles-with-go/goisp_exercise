// -- ****************************************************************/
// -- ****************************************************************/
// --                                                 ,,             */
// --          `7MMM.     ,MMF'         `7MMF'        db             */
// --            MMMb    dPMM             MM                         */
// --            M YM   ,M MM   .gP"Ya    MM        `7MM             */
// --            M  Mb  M' MM  ,M'   Yb   MM          MM             */
// --            M  YM.P'  MM  8M""""""   MM      ,   MM             */
// --            M  `YM'   MM  YM.    ,   MM     ,M   MM             */
// --          .JML. `'  .JMML. `Mbmmd' .JMMmmmmMMM .JMML.           */
// -- ****************************************************************/
// -- ****************************************************************/
/* -- ****************************************************************/
/* --                    procesador_test.go                          */
/* --                                                                */
/* --  Descripcion: Contiene las pruebas unitarias del ejercicio     */
/* --                                                                */
/* --   @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/* --                                                                */
/* --  © 2022 - Mercado Libre - Desafio Tecnico SOLID                */
/* -- ****************************************************************/
package test

import (
	"fmt"
	"testing"

	"gitlab.com/rodrigoghm/goisp_ejercicio/class/idioma"
	"gitlab.com/rodrigoghm/goisp_ejercicio/class/procesadorTexto"
)

func TestProcesadorSimple(t *testing.T) {
	var pt procesadorTexto.ProcesadorTexto
	pt.Nueva("No")
	pt.Nueva("himporta")
	pt.Nueva("la")
	pt.Nueva("Hortografia")
	expect := "No himporta la Hortografia"
	if expect != pt.ConcatTexto() {
		t.Errorf("Cadena no es igual. \n Valor Esperado ===> [%s] \n Valor Devuelto ===> [%s]", expect, pt.ConcatTexto())
	}
}

func TestProcesadorConIdioma(t *testing.T) {
	var pt procesadorTexto.ProcesadorTexto
	pt.Nueva("Tenho")
	pt.Nueva("fome")
	expect := "Tenho fome"
	if expect != pt.ConcatTexto() {
		t.Errorf("Cadena no es igual. \n Valor Esperado ===> [%s] \n Valor Devuelto ===> [%s]", expect, pt.ConcatTexto())
	}

	// - chequea el idioma
	var z idioma.IdiomaPO
	z.GenerateDiccionario()
	fmt.Println("Antes de hacer call ===> ", z.Language)
	if !pt.IsCorrecto(z.Language) {
		t.Errorf("Cadenas Invalidas")
	}
}
